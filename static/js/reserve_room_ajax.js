$("#create_from_poll").submit(f = function (e) {

    e.preventDefault();

    var form = $(this);
    var form_data = form.serialize();
    var url = form.attr('action');
    form.find(':input:not(:disabled)').prop('disabled', true);
    $('#room_list_loading').attr('hidden', true);
    $('#cancel_btn').attr('hidden', false);

    current_request = $.ajax({
        type: "POST",
        url: url,
        tryCount: 0,
        retryLimit: 2,
        data: form_data,
        success: function (data) {
            window.location.href = data['url'];
        },

        error: function (response) {
            alert(response.responseJSON['err'].toString());
            this.tryCount++;
            if (this.tryCount <= this.retryLimit) {
                current_request = $.ajax(this);
                return
            } else {
                form.find(':input:disabled').prop('disabled', false);
                $('#room_list_loading').attr('hidden', true);
                $('#cancel_btn').attr('hidden', true);
            }

        },
    });
});

$('#cancel_btn').on("click", function () {
    current_request.abort()
});