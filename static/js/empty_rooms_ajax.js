var current_request = null;

$(".js-poll-item").change(function () {
    $('#room_list').empty();
    $('#room_list_loading').attr('hidden', false);
    $('#submit_button').attr('disabled', true);
    current_request = $.ajax({
        url: '/reservation/available-rooms/',
        data: {
            'poll_item_id': this.value
        },
        dataType: 'json',

        beforeSend: function () {
            if (current_request != null) {
                current_request.abort();
            }
        },

        success: function (data) {
            let room_list = $('#room_list').removeClass('bg-info bg-danger text-light');
            if (!data['rooms'].length) {

            }
            for (var i in data['rooms']) {
                var room_number = data['rooms'][i];
                room_list.append(new Option(room_number, room_number));
                $('#submit_button').attr('disabled', false);
            }
        },

        error: function (response) {
            if (response.statusText == 'abort') {
                return;
            }
            let room_list = $('#room_list').removeClass('bg-info');
            room_list.addClass('bg-danger text-light');
            room_list.append(new Option(response.responseJSON['err']));
        },

        complete: function () {
            $('#room_list_loading').attr('hidden', true);
        }
    });
});