# SE_Project

This is the mono-repo of Jalas

## Celery

To run celery on development env, run the command below

```bash
$ celery -A jalas worker -EB -l info
```