from datetime import datetime
from typing import List, Union

from django.db.models import QuerySet
from django.template.loader import render_to_string
from requests import HTTPError, ConnectionError, ConnectTimeout

from reservation.clients.reservation_client import FetchRoomsClient, ReserveRoomClient
from reservation.exceptions import LogicException
from reservation.models import Room, Poll, PollItem
from utilities import mail_sender


def fetch_available_rooms(start: datetime, end: datetime) -> List[Room]:
    http_client = FetchRoomsClient(start, end)
    try:
        response = http_client.request_for_dict(fail_silently=False)
    except HTTPError as e:
        status = e.response.status_code
        if status == 500:
            exception_msg = 'در حال حاضر سامانه‌ی رزرو جلسات در دسترس نیست'
        else:
            exception_msg = 'خطایی پیش آمده است. لطفا مجددا تلاش کنید'
        raise LogicException(exception_msg)
    except (ConnectionError, ConnectTimeout):
        raise LogicException('برقراری ارتباط با سامانه رزرواسیون امکان‌پذیر نیست.')
    return [Room(number) for number in sorted(response['availableRooms'])]


def reserve_room(username: str, room_id: [int, str], start: datetime, end: datetime):
    http_client = ReserveRoomClient(username, room_id, start, end)
    try:
        http_client.request(fail_silently=False)
        return
    except HTTPError as e:
        status = e.response.status_code

        if status == 404:
            exception_msg = 'اتاق مورد نظر وجود ندارد.'
        elif status == 400:
            exception_msg = 'خطایی پیش آمده که به سرعت در حال رفع آن هستیم!'
        else:
            exception_msg = 'در حال حاضر سامانه رزواسیون در دسترس نیست'
    except (ConnectionError, ConnectTimeout):
        exception_msg = 'برقراری ارتباط با سامانه‌ی رزورواسیون امکان پذیر نیست. لطفا بعدا تلاش ' \
                        'کنید'
    raise LogicException(exception_msg)


def finalize_poll(poll: Poll):
    if not hasattr(poll, 'appointment'):
        return
    for invitation in poll.invitations.filter(user__appointment_notification=True):
        rendered_email = render_to_string('emails/appointment_notification.html',
                                          context={'poll': poll, 'invitation': invitation})
        mail_sender.send_email('برگزاری جلسه', rendered_email, [invitation.user.email])


def notify_poll_item_adds(poll_items: Union[QuerySet, List[PollItem]]):
    for invitation in poll_items[0].poll.invitations.filter(user__poll_item_add_notification=True):
        rendered_email = render_to_string('emails/add_poll_item_notification.html',
                                          context={'invitation': invitation})
        mail_sender.send_email('اضافه شدن گزینه‌های حضور', rendered_email, [invitation.user.email])


def on_item_delete(poll_item: PollItem):
    for vote in poll_item.vote_set.filter(invitation__user__poll_item_delete_notification=True):
        rendered_email = render_to_string('emails/delete_poll_item_notification.html',
                                          context={'invitation': vote.invitation})
        mail_sender.send_email('‌حذف گزینه‌ی حضور', rendered_email, [vote.invitation.user.email])


def on_poll_cancel(poll):
    rendered_email = render_to_string('emails/cancel_poll_notification.html',
                                      context={'poll': poll})
    receivers = list(poll.invitations.
                     filter(user__poll_cancel_notification=True).
                     values_list('user__email', flat=True))
    mail_sender.send_email('لغو نظرسنجی', rendered_email, receivers)


def on_appointment_cancel(appointment):
    rendered_email = render_to_string('emails/cancel_appointment_notification.html',
                                      context={'poll': appointment.poll})
    receivers = list(appointment.poll.invitations.
                     filter(user__appointment_cancel_notification=True).
                     values_list('user__email', flat=True))
    mail_sender.send_email('لغو جلسه', rendered_email, receivers)
