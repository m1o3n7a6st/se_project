class UnavailableReservationSystemException(Exception):
    def __init__(self, msg):
        self.msg = msg


class LogicException(Exception):
    def __init__(self, msg):
        self.msg = msg
