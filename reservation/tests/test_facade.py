from datetime import datetime, date, time

import mock
from django.test import TestCase, tag
from django.utils import timezone
from requests import HTTPError, Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST, \
    HTTP_404_NOT_FOUND

from accounts.models import User
from appointment.models import Appointment, PollInvitation
from reservation import facade
from reservation.exceptions import LogicException
from reservation.models import Poll, PollItem
from utilities import mail_sender
from utilities.base_http_client import BaseHttpClient


@tag('integration')
class FetchAvailableRoomsTest(TestCase):
    def setUp(self):
        super().setUp()
        self.start = datetime(2019, 1, 1, 14, 30)
        self.end = datetime(2019, 1, 1, 16, 5)

    @mock.patch.object(BaseHttpClient, 'request_for_dict')
    def test_with_successful_client_result(self, mocked_request):
        mocked_request.return_value = {
            'availableRooms': [1, 2, 5, 7]
        }

        actual_room_ids = [room.number for room in
                           facade.fetch_available_rooms(self.start, self.end)]

        self.assertListEqual([1, 2, 5, 7], actual_room_ids)

    @mock.patch.object(BaseHttpClient, 'request_for_dict')
    def test_when_http_client_raises_for_reservation_system_error(self, mocked_request):
        mocked_response = Response()
        mocked_response.status_code = HTTP_500_INTERNAL_SERVER_ERROR
        mocked_request.side_effect = HTTPError(response=mocked_response)
        with self.assertRaises(LogicException) as context:
            facade.fetch_available_rooms(self.start, self.end)
            self.assertIn('در دسترس نیست', context.exception.msg)

    @mock.patch.object(BaseHttpClient, 'request_for_dict')
    def test_when_http_client_raises_for_unavailable_reservation_system(
            self, mocked_request):
        mocked_response = Response()
        mocked_response.status_code = HTTP_400_BAD_REQUEST
        mocked_request.side_effect = HTTPError(response=mocked_response)
        with self.assertRaises(LogicException) as context:
            facade.fetch_available_rooms(self.start, self.end)
            self.assertIn('دوباره تلاش کنید', context.exception.msg)


@tag('integration')
class ReserveRoomTest(TestCase):
    def setUp(self):
        super().setUp()
        self.username = 'username1'
        self.room_id = 4
        self.start = datetime(2019, 1, 1, 14, 30)
        self.end = datetime(2019, 1, 1, 16, 5)

    @staticmethod
    def _create_side_effect(status_code):
        mocked_response = Response()
        mocked_response.status_code = status_code
        return HTTPError(response=mocked_response)

    @mock.patch.object(BaseHttpClient, 'request')
    def test_with_successful_response(self, mocked_request):
        facade.reserve_room(self.username, self.room_id, self.start, self.end)
        mocked_request.assert_called_once()

    @mock.patch.object(BaseHttpClient, 'request')
    def test_when_room_does_not_exist(self, mocked_request):
        mocked_request.side_effect = self._create_side_effect(HTTP_404_NOT_FOUND)
        with self.assertRaises(LogicException) as context:
            facade.reserve_room(self.username, self.room_id, self.start, self.end)
            self.assertIn('وجود ندارد', context.exception.msg)

    @mock.patch.object(BaseHttpClient, 'request')
    def test_when_reservation_system_fails(self, mocked_request):
        mocked_request.side_effect = self._create_side_effect(HTTP_500_INTERNAL_SERVER_ERROR)
        with self.assertRaises(LogicException) as context:
            facade.reserve_room(self.username, self.room_id, self.start, self.end)
            self.assertIn('در دسترس نیست', context.exception.msg)


@tag('unit')
class FinalizePollTest(TestCase):
    def setUp(self):
        self.creator = User.objects.create(username='creator', email='creator@mail.com')
        self.invited_user = User.objects.create(username='invited', email='invited@mail.com')
        self.poll = Poll.objects.create(title='The title', creator=self.creator,
                                        deadline=timezone.now())
        self.invitation = PollInvitation.objects.create(user=self.invited_user, poll=self.poll)
        self.poll_item = PollItem.objects.create(
            poll=self.poll, date=date(2019, 1, 1), start_at=time(12), end_at=time(13))

    @mock.patch.object(mail_sender, 'send_email')
    def test_fails_silently_when_poll_has_not_appointment(self, mocked_send_email):
        facade.finalize_poll(self.poll)
        mocked_send_email.assert_not_called()

    @mock.patch.object(mail_sender, 'send_email')
    def test_fails_silently_when_poll_has_appointment(self, mocked_send_email):
        Appointment.objects.create(poll_item=self.poll_item, poll=self.poll, room_number='210')
        mocked_send_email.reset_mock()
        self.poll.refresh_from_db()
        facade.finalize_poll(self.poll)
        mocked_send_email.assert_called_once()
