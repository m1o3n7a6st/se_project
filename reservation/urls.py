from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from reservation.views import PollDetailsView, AvailableRoomsView, \
    PollCreateView, VoteCreateView, CancelPollView, CommentsView, \
    AddItem, CommentsViewUpdate, CommentsViewDelete, PollItemDelete, ReplyCreateView

app_name = 'reservation'
urlpatterns = [
    path('polls/<int:pk>/cancel/', CancelPollView.as_view(), name='cancel-poll'),
    path('polls/<int:pk>/comments/', CommentsView.as_view(), name='poll-comments'),
    path('polls/<int:pk>/', PollDetailsView.as_view(), name='poll-details'),
    path('polls/<int:pk>/additem/', AddItem.as_view(), name='add-item'),
    path('polls/new/', PollCreateView.as_view(), name='poll-create'),
    path('<int:pk>/cancel/', PollItemDelete.as_view(), name='poll-item-delete'),
    path('available-rooms/', AvailableRoomsView.as_view()),
    path('<uuid:uuid>/', csrf_exempt(VoteCreateView.as_view()), name='vote-details'),
    path('comment-edit/<int:pk>/', CommentsViewUpdate.as_view(), name='comment-update'),
    path('comment-delete/<int:pk>/', CommentsViewDelete.as_view(), name='comment-delete'),
    path('reply-create/<int:pk>/', ReplyCreateView.as_view(), name='reply-create'),
]
