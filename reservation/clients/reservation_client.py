from datetime import datetime

from django.conf import settings

from utilities.base_http_client import BaseHttpClient


class FetchRoomsClient(BaseHttpClient):
    def __init__(self, start: datetime, end: datetime):
        super().__init__()
        self.start = start.isoformat()
        self.end = end.isoformat()

    def _get_path(self):
        return '/available_rooms/'

    def _get_method(self):
        return self.GET

    def _get_server_address(self):
        return settings.RESERVATION_SERVER_ADDRESS

    def _get_url_params(self):
        return {
            'start': self.start,
            'end': self.end,
        }


class ReserveRoomClient(BaseHttpClient):
    def __init__(self, username: str, room_id: int, start: datetime, end: datetime):
        super().__init__()
        self.room_id = room_id
        self.username = username
        self.start = start.isoformat()
        self.end = end.isoformat()

    def _get_path(self):
        return f'/rooms/{self.room_id}/reserve/'

    def _get_method(self):
        return self.POST

    def _get_server_address(self):
        return settings.RESERVATION_SERVER_ADDRESS

    def _get_data(self):
        return {
            'username': self.username,
            'start': self.start,
            'end': self.end
        }
