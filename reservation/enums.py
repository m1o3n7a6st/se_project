PollStateChoices = [
    ('WAITING_FOR_VOTE', 'منتظر ثبت رای'),
    ('RESERVING', 'در حال رزرو'),
    ('CANCELLED', 'کنسل شده'),
]

VoteStateChoices = [
    ('yes', 'مخالف'),
    ('no', 'موافق'),
    ('abstainer', 'ممتنع'),
]
