import re
from datetime import datetime

from django.db import models
from django.template.loader import render_to_string
from django.utils import timezone

from accounts.models import User
from reservation import facade
from reservation.enums import PollStateChoices, VoteStateChoices
from utilities import mail_sender


class Poll(models.Model):
    title = models.CharField(max_length=100, verbose_name='عنوان')
    created_at = models.DateTimeField(default=timezone.now)
    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='created_polls')
    last_retrieved_at = models.DateTimeField(blank=True, null=True)
    state = models.CharField(choices=PollStateChoices, max_length=40,
                             default='WAITING_FOR_VOTE')
    deadline = models.DateTimeField()

    def __str__(self):
        return self.title

    def is_cancelled(self):
        return self.state == 'CANCELLED'

    def has_appointment(self):
        return hasattr(self, 'appointment')


class PollItem(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    date = models.DateField()
    start_at = models.TimeField()
    end_at = models.TimeField()

    class Meta:
        unique_together = ['poll', 'start_at', 'end_at', 'date']

    def __str__(self):
        return f'{self.poll.title} {self.date} {self.start_at} - {self.end_at}'

    def delete(self, using=None, keep_parents=False):
        facade.on_item_delete(self)
        super().delete(using, keep_parents)

    @property
    def start(self):
        return datetime.combine(self.date, self.start_at)

    @property
    def end(self):
        return datetime.combine(self.date, self.end_at)

    def agreed_votes(self):
        return self.vote_set.filter(state='yes')

    def disagreed_votes(self):
        return self.vote_set.filter(state='no')

    def abstainer_votes(self):
        return self.vote_set.filter(state='abstainer')


class Vote(models.Model):
    invitation = models.ForeignKey('appointment.PollInvitation', on_delete=models.CASCADE)
    state = models.CharField(choices=VoteStateChoices, max_length=32)
    poll_item = models.ForeignKey(PollItem, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ['poll_item', 'invitation']


class Room:
    def __init__(self, number):
        self.number = number


class Comment(models.Model):
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    poll = models.ForeignKey('reservation.Poll', on_delete=models.CASCADE, blank=True, null=True)
    text = models.CharField(max_length=256)
    created_at = models.DateTimeField(default=timezone.now)
    root_comment = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)

    def save(self, *args, **kwargs):
        self._notify_mentioned_people()
        return super().save(*args, **kwargs)

    def get_root_poll(self):
        return self.poll or self.root_comment.get_root_poll()

    def get_safe_text(self) -> str:
        return re.sub(r'([\w.-]+@[\w.-]+)', r'<a href="#" class="text-info">\1</a>', self.text)

    def _extract_mentioned_emails(self):
        return re.findall(r'[\w.-]+@[\w.-]+', self.text)

    def _notify_mentioned_people(self):
        from appointment.models import PollInvitation

        for email in self._extract_mentioned_emails():
            invitation = PollInvitation.objects. \
                filter(poll=self.get_root_poll(), user__email=email). \
                filter(user__mention_notification=True).first()
            if invitation is None:
                continue
            rendered_email = render_to_string('emails/mention_notification.html',
                                              context={'comment': self, 'invitation': invitation})
            mail_sender.send_email('منشن شدید', rendered_email, [email])
