from django.contrib import admin

from .models import Poll, Vote, PollItem, Comment


class PollItemInline(admin.StackedInline):
    model = PollItem
    extra = 1


class PollAdmin(admin.ModelAdmin):
    inlines = [PollItemInline]


admin.site.register(Poll, PollAdmin)
admin.site.register(Vote)
admin.site.register(PollItem)
admin.site.register(Comment)
