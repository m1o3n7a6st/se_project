import re

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Field
from django import forms
from django.contrib.admin.widgets import AdminTimeWidget
from django.core.exceptions import ValidationError
from django.forms import TimeField, inlineformset_factory
from django.views.generic.dates import timezone_today
from jalali_date.fields import JalaliDateField, JalaliDateTimeField
from jalali_date.widgets import AdminJalaliDateWidget, AdminSplitJalaliDateTime

from reservation.models import PollItem, Vote, Poll


class PollCreateForm(forms.ModelForm):
    deadline = JalaliDateTimeField(label='ددلاین',
                                   widget=AdminSplitJalaliDateTime(attrs={'autocomplete': 'off'}))

    class Meta:
        model = Poll
        fields = ['title', 'deadline']


class PollItemForm(forms.ModelForm):
    date = JalaliDateField(
        label='تاریخ',
        widget=AdminJalaliDateWidget(attrs={'autocomplete': 'off'}))
    start_at = TimeField(
        label='شروع',
        widget=AdminTimeWidget(attrs={'class': 'timepicker', 'autocomplete': 'off'}))
    end_at = TimeField(
        label='پایان',
        widget=AdminTimeWidget(attrs={'class': 'timepicker', 'autocomplete': 'off'}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        formtag_prefix = re.sub('-[0-9]+$', '', kwargs.get('prefix', ''))

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Row(
                Field('date'),
                Field('start_at'),
                Field('end_at'),
                Field('DELETE'),
                css_class='formset_row-{}'.format(formtag_prefix)
            )
        )

    class Meta:
        model = PollItem
        fields = ['date', 'start_at', 'end_at']

    def clean_end_at(self):
        start_at = self.cleaned_data['start_at']
        end_at = self.cleaned_data['end_at']
        if end_at <= start_at:
            raise ValidationError('زمان پایان باید بزرگتر از زمان آغاز باشد')
        return end_at

    def clean_date(self):
        date = self.cleaned_data['date']
        if date < timezone_today():
            raise ValidationError('تاریخ انتخابی نمی‌تواند در گذشته باشد')
        return date


PollItemFormSet = inlineformset_factory(
    Poll, PollItem, form=PollItemForm, extra=0, can_delete=True, min_num=2, validate_min=True
)

AddItemFormSet = inlineformset_factory(
    Poll, PollItem, form=PollItemForm, extra=1, can_delete=True)


class UserForm(forms.ModelForm):
    class Meta:
        model = Vote
        fields = ['poll_item']
