from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.db import transaction
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.views import View
from django.views.generic import DetailView, CreateView, UpdateView, DeleteView
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR

from appointment.forms import PollInvitationFormSet
from appointment.models import PollInvitation, Appointment
from reservation import facade
from reservation.exceptions import LogicException
from reservation.models import Poll, PollItem, Vote, Comment
from utilities import mail_sender
from .forms import PollItemFormSet, AddItemFormSet, PollCreateForm


class AddItem(PermissionRequiredMixin, View):
    def has_permission(self):
        self.poll = get_object_or_404(Poll, pk=self.kwargs.get('pk'))
        return self.request.user.is_authenticated

    def post(self, request, *args, **kwargs):
        formset = AddItemFormSet(request.POST, instance=self.poll)
        if formset.is_valid():
            added_poll_items = formset.save()
            facade.notify_poll_item_adds(added_poll_items)
            return redirect(reverse('reservation:poll-details', kwargs={'pk': self.poll.pk}))
        messages.error(request, 'لطفا خطاهای زیر را یرطرف کنید!')
        return render(request, 'reservations/poll_details_view.html', context={
            'poll': self.poll,
            'poll_items': formset
        })


class PollItemDelete(PermissionRequiredMixin, DeleteView):
    model = PollItem

    def has_permission(self):
        poll_item = get_object_or_404(PollItem, pk=self.kwargs.get('pk'))
        return self.request.user == poll_item.poll.creator

    def get(self, request, *args, **kwargs):
        messages.success(request, 'گزینه‌ی مورد نظر با موفقیت حذف شد')
        return self.delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('reservation:poll-details', kwargs={'pk': self.object.poll_id})


class PollDetailsView(PermissionRequiredMixin, DetailView):
    template_name = 'reservations/poll_details_view.html'
    model = Poll
    context_object_name = 'poll'

    def has_permission(self):
        obj = self.get_object()
        allowed_user_ids = list(obj.invitations.values_list('user', flat=True))
        return self.request.user.id in allowed_user_ids

    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        if obj and not hasattr(obj, 'appointment'):
            obj.last_retrieved_at = timezone.now()
            obj.save()
        return obj

    def get_context_data(self, **kwargs):
        obj = self.get_object()
        context = super().get_context_data(**kwargs)
        context['can_comment'] = not obj.has_appointment() and not obj.is_cancelled()
        context['poll_items'] = AddItemFormSet()
        context['editable'] = context['can_comment'] and self.request.user == obj.creator
        return context


class CommentsView(PermissionRequiredMixin, CreateView):
    model = Comment
    fields = ['text']

    def get_success_url(self):
        return reverse('reservation:poll-details', kwargs={'pk': self.object.poll.pk})

    def has_permission(self):
        poll = get_object_or_404(Poll, pk=self.kwargs['pk'])
        return self.request.user.id in list(poll.invitations.values_list('user', flat=True))

    def form_valid(self, form):
        form.instance.poll_id = self.kwargs['pk']
        form.instance.user = self.request.user
        return super().form_valid(form)


class CommentsViewUpdate(PermissionRequiredMixin, UpdateView):
    model = Comment
    fields = ['text']
    template_name = 'reservations/edit_comment.html'

    def has_permission(self):
        comment = get_object_or_404(Comment, pk=self.kwargs['pk'])
        return self.request.user == comment.user

    def post(self, request, *args, **kwargs):
        messages.success(request, 'نظر شما بازنگری شد')
        return super().post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('reservation:poll-details', kwargs={'pk': self.object.get_root_poll().pk})


class CommentsViewDelete(PermissionRequiredMixin, DeleteView):
    model = Comment

    def has_permission(self):
        comment = get_object_or_404(Comment, pk=self.kwargs['pk'])
        return self.request.user == comment.user

    def get(self, request, *args, **kwargs):
        messages.success(request, 'نظر شما حذف شد')
        return super().delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('reservation:poll-details', kwargs={'pk': self.object.poll_id})


class ReplyCreateView(PermissionRequiredMixin, CreateView):
    model = Comment
    fields = ['text']
    template_name = 'reservations/reply_create_view.html'

    def has_permission(self):
        self.root_comment = get_object_or_404(Comment, id=self.kwargs['pk'])
        allowed_user_ids = list(
            self.root_comment.get_root_poll().invitations.values_list('user', flat=True))
        return self.request.user.id in allowed_user_ids

    def form_valid(self, form):
        form.instance.root_comment = self.root_comment
        form.instance.user = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['root_comment'] = self.root_comment
        return context

    def get_success_url(self):
        messages.success(self.request, 'با موفقیت ریپلای کردید')
        return reverse('reservation:poll-details', kwargs={'pk': self.object.get_root_poll().id})


class AvailableRoomsView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        poll_item_id = request.GET.get('poll_item_id')
        poll_item = get_object_or_404(PollItem, id=poll_item_id)
        try:
            rooms = facade.fetch_available_rooms(poll_item.start, poll_item.end)
        except LogicException as e:
            return JsonResponse(status=HTTP_500_INTERNAL_SERVER_ERROR, data={
                'err': e.msg
            })

        return JsonResponse(data={
            'rooms': [room.number for room in rooms]
        })


class PollCreateView(CreateView):
    model = Poll
    form_class = PollCreateForm
    template_name = 'reservations/poll_create_view.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.POST:
            context['poll_items'] = PollItemFormSet(self.request.POST)
            context['poll_invitations'] = PollInvitationFormSet(self.request.POST)
        else:
            context['poll_items'] = PollItemFormSet()
            context['poll_invitations'] = PollInvitationFormSet()
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        poll_items = context['poll_items']
        invitations = context['poll_invitations']
        with transaction.atomic():
            form.instance.creator = self.request.user
            self.object = form.save()
            if poll_items.is_valid() and invitations.is_valid():
                poll_items.instance = self.object
                poll_items.save()
                invitations.instance = self.object
                invitations.save()
                try:
                    PollInvitation.objects.get_or_create(user=self.request.user, poll=self.object)
                except Exception:
                    pass
            else:
                return self.form_invalid(form)
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(self.request, 'نظرستجی با موفقیت ایجاد شد!')
        return reverse('reservation:poll-details', kwargs={'pk': self.object.pk})


class VoteCreateView(View):
    def get(self, request, uuid):
        invitation = get_object_or_404(PollInvitation, uuid=uuid)
        login(request, invitation.user)
        if invitation.poll.has_appointment():
            return HttpResponseRedirect(
                reverse('reservation:poll-details', kwargs={'pk': invitation.poll_id}))
        return render(request, 'reservations/vote_create_view.html', context={
            'poll': invitation.poll,
            'votes': invitation.vote_set.all(),
        })

    @transaction.atomic
    def post(self, request, uuid):
        invitation = get_object_or_404(PollInvitation, uuid=uuid)
        if hasattr(invitation.poll, 'appointment'):
            return HttpResponseRedirect(reverse('reservation:poll-details'))

        if not self._data_is_valid(invitation):
            messages.error(request, 'برای هر ردیف باید نظر خود را ثبت کنید')
            return render(request, 'reservations/vote_create_view.html', context={
                'poll': invitation.poll
            })
        self._create_new_votes(invitation)
        messages.success(request, 'نظر شما ثبت شده است. منتظر قطعی شدن جلسه بمانید.')
        return self.get(request, uuid)

    @staticmethod
    def _notify_creator(votes, invitation):
        if not invitation.poll.creator.vote_add_notification:
            return
        rendered_email = render_to_string('emails/votes_notification.html', context={
            'votes': votes
        })
        mail_sender.send_email('ثبت رای جدید', rendered_email, [invitation.poll.creator.email])

    def _data_is_valid(self, invitation):
        given_item_ids = sorted([int(key) for key in dict(self.request.POST)])
        required_item_ids = sorted(list(invitation.poll.pollitem_set.values_list('id', flat=True)))
        print('given_item_ids: ', given_item_ids)
        print('required_item_ids: ', required_item_ids)
        if given_item_ids != required_item_ids:
            return False
        return True

    def _create_new_votes(self, invitation):
        invitation.vote_set.all().delete()

        data = dict(self.request.POST)
        votes = [Vote(invitation=invitation, state=data[poll_item][0], poll_item_id=poll_item)
                 for poll_item in data]
        Vote.objects.bulk_create(votes)
        self._notify_creator(votes, invitation)


class CancelPollView(PermissionRequiredMixin, View):
    def has_permission(self):
        self.poll = get_object_or_404(Poll, pk=self.kwargs['pk'])
        return self.poll.creator == self.request.user and not self.poll.has_appointment()

    def get(self, request, *args, **kwargs):
        self.poll.state = 'CANCELLED'
        self.poll.save()
        Appointment.objects.filter(poll=self.poll).delete()
        facade.on_poll_cancel(self.poll)
        messages.success(request, 'نظرسنجی لغو شد')
        return HttpResponseRedirect(reverse('reservation:poll-details',
                                            kwargs={'pk': self.poll.pk}))
