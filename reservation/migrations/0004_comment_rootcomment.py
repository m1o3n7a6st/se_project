# Generated by Django 2.2.7 on 2020-01-07 17:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0003_remove_vote_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='rootComment',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='reservation.Comment'),
            preserve_default=False,
        ),
    ]
