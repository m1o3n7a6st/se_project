from celery import shared_task
from django.utils import timezone

from appointment.models import Appointment
from reservation import facade
from reservation.models import Poll


@shared_task
def finalize_polls():
    now = timezone.now()
    polls = Poll.objects.filter(deadline__lte=now, appointment__isnull=True,
                                state='WAITING_FOR_VOTE')
    print(f'Found {polls.count()} polls to finalize for deadline')
    for poll in polls:
        items = list(poll.pollitem_set.all())
        items = sorted(items, key=lambda x: x.vote_set.filter(state='yes').count(), reverse=True)
        Appointment.objects.create(poll_item=items[0], poll=poll)
        facade.finalize_poll(poll)
        print(f'finalized poll with id={poll.id} after its deadline')
