from django.urls import path

from appointment.views import IndexView, IndexViewOld, DetailView, CreateFromPoll, \
    CancelAppointmentView, PollInvitationCreateView, PollInvitationDeleteView

app_name = 'appointment'
urlpatterns = [
    path('home/', IndexView.as_view(), name='home'),
    path('cancel-appointment/<int:pk>/', CancelAppointmentView.as_view(),
         name='cancel-appointment'),
    path('old/', IndexViewOld.as_view(), name='old'),
    path('<int:pk>/', DetailView.as_view(), name='detail'),
    path('from-polls/', CreateFromPoll.as_view(), name='create-from-poll'),
    path('poll-invitations/create/<int:pk>/', PollInvitationCreateView.as_view(),
         name='create-poll-invitation'),
    path('poll-invitations/delete/<int:pk>/', PollInvitationDeleteView.as_view(),
         name='delete-poll-invitation'),
]
