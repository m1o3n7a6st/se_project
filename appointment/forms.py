import re

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Row, Field
from django import forms
from django.core.exceptions import ValidationError
from django.forms import inlineformset_factory, ModelForm

from accounts.models import User
from appointment.models import PollInvitation
from reservation.models import Poll


class PollInvitationAddForm(ModelForm):
    email = forms.EmailField(label='ایمیل')

    def __init__(self, *args, **kwargs):
        poll = kwargs.pop('poll')
        super().__init__(*args, **kwargs)
        self.poll = poll

    def clean_email(self):
        email = self.cleaned_data['email']
        if self.poll.invitations.filter(user__email=email).exists():
            raise ValidationError('در حال حاضر فرد مورد نظر دعوت شده است')
        return email

    def save(self, commit=True):
        email = self.cleaned_data['email']
        user, _ = User.objects.get_or_create(email=email, defaults={
            'username': email
        })
        self.instance.user = user
        return super().save(commit)

    class Meta:
        model = PollInvitation
        fields = ['email']


class PollInvitationForm(ModelForm):
    email = forms.EmailField(label='ایمیل')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        formtag_prefix = re.sub('-[0-9]+$', '', kwargs.get('prefix', ''))

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Row(
                Field('email'),
                Field('DELETE'),
                css_class='formset_row-{}'.format(formtag_prefix)
            )
        )

    def save(self, commit=True):
        email = self.cleaned_data['email']
        user, _ = User.objects.get_or_create(email=email, defaults={
            'username': email
        })
        self.instance.user = user
        return super().save(commit)

    class Meta:
        model = PollInvitation
        fields = []


PollInvitationFormSet = inlineformset_factory(
    Poll, PollInvitation, form=PollInvitationForm, extra=0, can_delete=True, min_num=2,
    validate_min=True
)
