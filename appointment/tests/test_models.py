from datetime import time, date

import mock
from django.test import TestCase, tag
from django.utils import timezone

from accounts.models import User
from appointment.models import PollInvitation, Appointment
from reservation.models import Poll, PollItem
from utilities import mail_sender


@tag('unit')
class PollInvitationTest(TestCase):
    def setUp(self):
        super().setUp()
        creator_user = User.objects.create(username='u1', email='creator@domain.com')
        self.invited_user = User.objects.create(username='u2', email='invited@domain.com')
        self.poll = Poll.objects.create(title='Test Poll', creator=creator_user,
                                        deadline=timezone.now())

    @mock.patch.object(mail_sender, 'send_email')
    def test_creating_new_object_sends_invitations_email(self, mocked_send_email):
        PollInvitation.objects.create(poll=self.poll, user=self.invited_user)
        mocked_send_email.assert_called_once()


@tag('unit')
class AppointmentTest(TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create(username='u1', email='test@domain.com')
        self.poll = Poll.objects.create(title='Test Poll', creator=user, deadline=timezone.now())
        self.poll_item = PollItem.objects.create(
            poll=self.poll, start_at=time(20, 30), end_at=time(21, 35), date=date(2019, 1, 1)
        )
        self.invitation = PollInvitation.objects.create(user=user, poll=self.poll)

    @mock.patch.object(mail_sender, 'send_email')
    def test_creating_new_object_sends_invitations_email(self, mocked_send_email):
        Appointment.objects.create(poll=self.poll, poll_item=self.poll_item, room_number='204')
        mocked_send_email.assert_called_once()
