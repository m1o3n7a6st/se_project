from datetime import datetime

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views import generic, View
from django.views.generic import CreateView, DeleteView
from rest_framework.status import HTTP_400_BAD_REQUEST

from appointment.forms import PollInvitationAddForm
from appointment.models import Appointment, PollInvitation
from reservation import facade
from reservation.exceptions import LogicException
from reservation.models import PollItem, Poll


class IndexView(PermissionRequiredMixin, generic.ListView):
    template_name = 'appointment/home.html'
    context_object_name = 'object'

    def has_permission(self):
        return self.request.user.is_authenticated

    def get_queryset(self):
        poll_created = Poll.objects.filter(
            creator__username=self.request.user.username,
            appointment__created_at__isnull=True).distinct()
        appointments = Appointment.objects.filter(
            poll_item__date__gt=datetime.now().date()).order_by('poll_item__date')
        poll_invited = PollInvitation.objects.filter(user=self.request.user, vote__isnull=True,
                                                     poll__appointment__isnull=True).distinct()
        return {'poll_created': poll_created,
                'appointment': appointments, 'pollInvited': poll_invited}


class IndexViewOld(generic.ListView):
    template_name = 'appointment/index.html'
    context_object_name = 'object'

    def get_queryset(self):
        appointments = Appointment.objects.filter(
            poll_item__date__lt=datetime.now().date()).order_by(
            'poll_item__date')[:10]
        return {'appointment': appointments}


class DetailView(PermissionRequiredMixin, generic.DetailView):
    model = Appointment
    template_name = 'appointment/detail.html'
    context_object_name = 'appointment'

    def has_permission(self):
        return self.request.user == self.get_object().poll.creator


class CreateFromPoll(LoginRequiredMixin, View):
    model = Appointment

    def post(self, request, *args, **kwargs):
        try:
            room_number = request.POST.get('room_number')
            poll_item = self._extract_poll_item(request)
            self._reserve_room(poll_item, room_number)
            poll_item.poll.refresh_from_db()
            if poll_item.poll.state != 'CANCELLED':
                Appointment.objects.create(
                    poll_item=poll_item,
                    poll=poll_item.poll,
                    room_number=room_number,
                )
        except LogicException as e:
            return JsonResponse(data={'err': e.msg}, status=HTTP_400_BAD_REQUEST)
        return JsonResponse(
            data={'url': reverse('reservation:poll-details', kwargs={'pk': poll_item.poll_id})})

    @staticmethod
    def _extract_poll_item(request):
        poll_item_id = request.POST.get('poll_item')
        poll_item = get_object_or_404(PollItem, id=poll_item_id)
        if poll_item.poll.creator != request.user or hasattr(poll_item.poll, 'appointment'):
            raise PermissionDenied()
        if poll_item.poll.state == 'CANCELLED':
            raise LogicException('این نظرسنجی کنسل شده است')
        poll_item.poll.state = 'RESERVING'
        poll_item.poll.save()
        return poll_item

    def _reserve_room(self, poll_item, room_number):
        facade.reserve_room(
            poll_item.poll.creator.username,
            room_number,
            poll_item.start,
            poll_item.end)


class CancelAppointmentView(PermissionRequiredMixin, View):
    def has_permission(self):
        self.appointment = get_object_or_404(Appointment, pk=self.kwargs['pk'])
        return self.appointment.poll.creator == self.request.user

    def get(self, request, *args, **kwargs):
        self.appointment.is_cancelled = True
        self.appointment.save()
        facade.on_appointment_cancel(self.appointment)
        messages.success(request, 'جلسه لغو شد')
        return HttpResponseRedirect(reverse('reservation:poll-details',
                                            kwargs={'pk': self.appointment.poll_id}))


class PollInvitationCreateView(PermissionRequiredMixin, CreateView):
    form_class = PollInvitationAddForm
    template_name = 'appointment/poll_invitation_create_view.html'

    def get_poll(self):
        return get_object_or_404(Poll, pk=self.kwargs['pk'])

    def has_permission(self):
        return self.get_poll().creator == self.request.user

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['poll'] = self.get_poll()
        return kwargs

    def form_valid(self, form):
        poll = self.get_poll()
        form.instance.poll = poll
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('reservation:poll-details', kwargs={'pk': self.get_poll().id})


class PollInvitationDeleteView(PermissionRequiredMixin, DeleteView):
    model = PollInvitation

    def has_permission(self):
        invitation = get_object_or_404(PollInvitation, pk=self.kwargs['pk'])
        return invitation.poll.creator == self.request.user

    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)

    def get_success_url(self):
        messages.success(self.request, 'کاربر مورد نظر با موفقیت حذف شد')
        return reverse('reservation:poll-details', kwargs={'pk': self.object.poll_id})
