import uuid

from django.db import models
from django.template.loader import render_to_string
from django.utils import timezone

from accounts.models import User
from reservation import facade
from reservation.models import Poll, PollItem
from utilities import mail_sender


class Appointment(models.Model):
    poll_item = models.ForeignKey(PollItem, on_delete=models.CASCADE)
    poll = models.OneToOneField(Poll, on_delete=models.CASCADE)
    room_number = models.CharField(max_length=64, blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    is_cancelled = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        created = self.pk is None
        super().save(*args, **kwargs)
        if created:
            self._on_create()

    def _on_create(self):
        facade.finalize_poll(self.poll)


class PollInvitation(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4)
    poll = models.ForeignKey('reservation.Poll', on_delete=models.CASCADE,
                             related_name='invitations')
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        created = self.pk is None
        super().save(*args, **kwargs)
        if created:
            self._on_create()

    def delete(self, *args, **kwargs):
        self._on_delete()
        return super().delete(*args, **kwargs)

    def _on_create(self):
        if not self.user.invitation_notification:
            return
        rendered_email = render_to_string('emails/poll_invitation.html', context={
            'invitation': self
        })
        mail_sender.send_email('نظرسنجی جلسه', rendered_email, [self.user.email])

    def _on_delete(self):
        if not self.user.invitation_delete_notification:
            return
        rendered_email = render_to_string('emails/invitation_delete_notification.html', context={
            'invitation': self
        })
        mail_sender.send_email('پس گرفتن دعوت', rendered_email, [self.user.email])
