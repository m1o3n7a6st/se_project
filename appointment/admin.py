from django.contrib import admin

from .models import Appointment, PollInvitation

admin.site.register(Appointment)
admin.site.register(PollInvitation)
