from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import path, include

urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),
    path('accounts/', include('accounts.urls')),
    path('appointment/', include('appointment.urls')),
    path('reservation/', include('reservation.urls')),
    path('monitoring/', include('monitoring.urls')),
    prefix_default_language=False)
