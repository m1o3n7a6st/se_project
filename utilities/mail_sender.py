import threading
from typing import List

from django.conf import settings
from django.core.mail import EmailMessage


def send_email(subject: str, html_content, bcc: List[str]):
    email = EmailMessage(subject, html_content, from_email=settings.EMAIL_HOST_USER, bcc=bcc)
    email.content_subtype = 'html'
    threading.Thread(target=email.send).start()
