import json
import logging
from abc import abstractmethod
from typing import Dict, Optional

import requests
from requests import Response
from requests.exceptions import RequestException


class _Unspecified:
    pass


UNSPECIFIED = _Unspecified()


def is_unspecified(some_value):
    return some_value is not None and isinstance(some_value, _Unspecified)


class BaseHttpClient:
    PATCH = 'patch'
    DELETE = 'delete'
    PUT = 'put'
    POST = 'post'
    GET = 'get'

    @abstractmethod
    def _get_path(self):
        raise NotImplementedError()

    @abstractmethod
    def _get_method(self):
        raise NotImplementedError()

    @abstractmethod
    def _get_server_address(self):
        raise NotImplementedError()

    def _get_auth(self):
        return None

    def _get_headers(self):
        return {'content-type': 'application/json'}

    def _get_data(self):
        return None

    def _get_url_params(self) -> Optional[Dict[str, str]]:
        return None

    def __get_prepared_data(self):
        if not hasattr(self, '__cached_data_holder'):
            self.__cached_data_holder = UNSPECIFIED
        if is_unspecified(self.__cached_data_holder):
            self.__cached_data_holder = self._get_data()
        if self._get_data() is None:
            return None
        return json.dumps(self.__cached_data_holder)

    def request(self, fail_silently=True) -> Response:
        """
        :rtype: requests.models.Response
        """
        try:
            logging.info('{class_name} sending a request'.format(class_name=self.__class__))
            response = {
                self.GET: requests.get,
                self.POST: requests.post,
                self.PUT: requests.put,
                self.PATCH: requests.patch,
                self.DELETE: requests.delete
            }[self._get_method()](
                url='{host}{path}'.format(host=self._get_server_address(), path=self._get_path()),
                auth=self._get_auth(),
                headers=self._get_headers(),
                params=self._get_url_params(),
                data=self.__get_prepared_data(),
                timeout=15
            )

            response.raise_for_status()
            return response
        except KeyError:
            raise ValueError('Method {method} not supported'.format(method=self._get_method()))
        except RequestException as e:
            if not fail_silently:
                raise e

    def request_for_dict(self, fail_silently=True):
        response = self.request(fail_silently=fail_silently)
        if response is None:
            return None
        return response.json()
