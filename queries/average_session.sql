SELECT time(avg(3660 * 24 *
                (julianday(Appointments.created_at) - julianday(Polls.last_retrieved_at))),
            'unixepoch') AS avg_session_time
FROM appointment_appointment AS Appointments
         JOIN reservation_poll AS Polls ON Appointments.poll_id = Polls.id;