from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    phoneNumber = models.CharField(max_length=11)
    invitation_notification = models.BooleanField(default=True)
    appointment_notification = models.BooleanField(default=True)
    poll_item_add_notification = models.BooleanField(default=True)
    poll_item_delete_notification = models.BooleanField(default=True)
    vote_add_notification = models.BooleanField(default=True)
    poll_cancel_notification = models.BooleanField(default=True)
    appointment_cancel_notification = models.BooleanField(default=True)
    mention_notification = models.BooleanField(default=True)
    invitation_delete_notification = models.BooleanField(default=True)

    def get_full_name(self):
        return super().get_full_name() or self.username
