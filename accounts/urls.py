from django.contrib.auth.views import LogoutView
from django.urls import path

from accounts.views import UserFormView, LoginView, NotificationPolicyView

app_name = 'accounts'
urlpatterns = [
    path('signup/', UserFormView.as_view(), name='register'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('notification-policy/', NotificationPolicyView.as_view(), name='notification-policy')
]
