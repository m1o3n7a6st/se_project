from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth import views as auth_views
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import UpdateView
from django.views.generic.base import View

from .forms import UserForm
from .models import User


class UserFormView(View):
    form_class = UserForm
    template_name = 'accounts/registration_form.html'

    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():
            user = form.save(commit=False)
            # cleaned (normalized)data
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()
            # return User objects if credntials are correct
            user = authenticate(username=username, password=password)

            if user is not None:

                if user.is_active:
                    login(request, user)
                    return redirect('appointment:home')

        return render(request, self.template_name, {'form': form})


class LoginView(auth_views.LoginView):
    redirect_authenticated_user = True

    def get_success_url(self):
        return reverse('appointment:home')


class NotificationPolicyView(LoginRequiredMixin, UpdateView):
    model = User
    template_name = 'accounts/notification_policy_view.html'
    fields = [
        'invitation_notification',
        'appointment_notification',
        'poll_item_add_notification',
        'poll_item_delete_notification',
        'vote_add_notification',
        'poll_cancel_notification',
        'appointment_cancel_notification',
        'mention_notification',
    ]

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        messages.success(self.request, 'سیاست اعلان با موفقیت به روز شد')
        return reverse('accounts:notification-policy')
