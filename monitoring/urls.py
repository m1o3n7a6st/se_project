from django.urls import path

from monitoring.views import QualityInUseView, PerformanceOverviewView

app_name = 'monitoring'
urlpatterns = [
    path('quality/', QualityInUseView.as_view(), name='quality'),
    path('performance/', PerformanceOverviewView.as_view(), name='performance')
]
