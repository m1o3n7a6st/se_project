import datetime
import math

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import F, Avg
from django.utils import timezone
from django.views.generic import TemplateView
from request_profiler.models import ProfilingRecord

from appointment.models import Appointment
from reservation.models import Poll


class QualityInUseView(PermissionRequiredMixin, TemplateView):
    template_name = 'monitoring/quality_in_use.html'

    def has_permission(self):
        return self.request.user.is_staff

    def get_context_data(self, **kwargs):
        data = {
            **self._get_average_appointment_time_creation_row(),
            **self._get_reserved_rooms_count_row(),
            **self._get_cancelled_polls(),
        }
        return {'data': data}

    @staticmethod
    def _get_average_appointment_time_creation_row():
        appointments = Appointment.objects.annotate(fetch_datetime=F('poll__last_retrieved_at'))
        try:
            total_time = sum([appointment.created_at - appointment.fetch_datetime
                              for appointment in appointments], datetime.timedelta())
            value = total_time / appointments.count()
        except ZeroDivisionError:
            value = 0
        return {
            'متوسط زمان ایجاد جلسه': value
        }

    @staticmethod
    def _get_reserved_rooms_count_row():
        return {
            'تعداد اتاق‌های رزرو شده توسط سیستم': Appointment.objects.count()
        }

    @staticmethod
    def _get_cancelled_polls():
        return {
            'تعداد جلسه‌های کنسل شده': Poll.objects.filter(state='CANCELLED').count()
        }


class PerformanceOverviewView(PermissionRequiredMixin, TemplateView):
    template_name = 'monitoring/performance_overview.html'

    def has_permission(self):
        return self.request.user.is_staff

    def get_context_data(self, **kwargs):
        records = ProfilingRecord.objects. \
            filter(start_ts__gte=timezone.now() - datetime.timedelta(hours=24))
        average_response_time_in_ms = records.aggregate(avg=Avg('duration'))['avg'] * 1000
        return {
            'throughput': records.count(),
            'response_time': math.floor(average_response_time_in_ms * 10) / 10
        }
